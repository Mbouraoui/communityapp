from django.shortcuts import render

# Create your views here.
from django.contrib.auth import logout, authenticate, login
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.urls import reverse
from django.views import generic
from django.views.generic import DetailView

from CommunityApp.forms import AddQuestionForm
from .models import *


def index(request):
    return render(request, 'index.html')

def questions_list(request):
    all_questions_list = Question.objects.all()
    return render(request, 'questions.html', {'questions_list': all_questions_list})

def question_details(request, qID):
    question = get_object_or_404(Question, pk=qID)
    tags = question.tags.all()
    return render(request,
                  'question_details.html',
                  {
                      'question': question,
                      'tags':tags
                   }
                  )


def add_question(request):
    if request.method == "GET":
        form = AddQuestionForm()
        return render(request, 'add_question.html', {'form': form})
    if request.method == "POST":
        form = AddQuestionForm(request.POST)
        if form.is_valid():
            postQuestion = form.save(commit=False)
            postQuestion.save()
            return HttpResponseRedirect(reverse('list'))
        else:
            return render(request, 'add_question.html',
                          {'msg_erreur': 'Erreur lors de la création du projet',
                           'form': form})
