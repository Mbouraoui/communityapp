from django import forms

from CommunityApp.models import Question


class AddQuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('questionTitle','questionText','publisher','tags')