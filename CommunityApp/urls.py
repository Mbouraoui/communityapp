from django.conf.urls import url
from django.urls import re_path

from CommunityApp import views

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^questions/$', views.questions_list, name='list'),
    re_path(r'^questions/(?P<qID>[0-9]+)/$', views.question_details, name='details'),
    re_path(r'^questions/add/$', views.add_question, name='addQuestion'),

]