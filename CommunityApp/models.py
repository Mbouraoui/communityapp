# A ajouter la fonctinnalité de marquer une publication comme inappropriée
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.db import models


def isValidEmail(email):
    if str(email).endswith("tek-up.tn") == False:
        raise ValidationError(
            ('%(email) is not a TEK-UP Email'),
            params={'email': email},
        )

class User(models.Model):
    userName = models.CharField(default="username", max_length=30)
    name = models.CharField(default="name", max_length=30)
    email = models.EmailField(default="user@tek-up.tn", validators=[isValidEmail])

    def __str__(self):
        return self.userName


class Member(User):
    def get_num_publications(self):
        return Question.objects.filter(publisher=self).count() or 0


class Tag(models.Model):
    tagText = models.CharField(max_length=20)

    def __str__(self):
        return self.tagText


class Question(models.Model):
    questionTitle = models.CharField(max_length=30, default="Publication")
    questionText = models.TextField(validators=[MinLengthValidator(30)])
    isClosed = models.BooleanField(default=False)
    isValid = models.BooleanField(default=False)

    reports = models.ManyToManyField(
        'Member',
        through='QuestionReportsByUsers',
        # added to differ with the lead relation
        related_name='user_reports',
        blank=True,
    )

    publisher = models.ForeignKey(Member, default=None, on_delete=models.DO_NOTHING)
    solution = models.OneToOneField('Answer', blank=True, null=True, on_delete=models.CASCADE)
    tags = models.ManyToManyField(
        Tag,
        through='TagsInPublication',
        # added to differ with the lead relation
        related_name='publication_tags',
        blank=False,
    )

    def __str__(self):
        return self.questionTitle + " Text: " + self.questionText


class Answer(models.Model):
    answerText = models.TextField(default=None)

    publication = models.ForeignKey(Question, default=None, on_delete=None, blank=True)
    commenter = models.ForeignKey(Member, default=None, on_delete=models.DO_NOTHING)
    likers = models.ManyToManyField(
        Member,
        through='LikesByUsers',
        # added to differ with the lead relation
        related_name='comment_likers',
        blank=True,
    )

    reports = models.ManyToManyField(
        'Member',
        through='AnswerReportsByUsers',
        # added to differ with the lead relation
        related_name='answer_user_reports',
        blank=True,
    )

    def __str__(self):
        return self.answerText



class LikesByUsers(models.Model):
    comment = models.ForeignKey(Answer, default=None,on_delete=models.DO_NOTHING)
    member = models.ForeignKey(Member, default=None, on_delete= models.DO_NOTHING)


class QuestionReportsByUsers(models.Model):
    user = models.ForeignKey(Member, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    motif = models.TextField('motif', max_length=100)

class AnswerReportsByUsers(models.Model):
    user = models.ForeignKey(Member, on_delete=models.CASCADE)
    question = models.ForeignKey(Answer, on_delete=models.CASCADE)
    motif = models.TextField('motif', max_length=100)


class TagsInPublication(models.Model):
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    publication = models.ForeignKey(Question, on_delete=models.CASCADE)

