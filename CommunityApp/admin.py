from django.contrib import admin

from .models import *


class TagInlines(admin.TabularInline):
    model = TagsInPublication
    extra = 0

class CommentInlines(admin.TabularInline):
    model = Answer
    extra = 0

class MemberAdmin(admin.ModelAdmin):
    list_display = ('userName', 'get_num_publications')

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('questionTitle', 'questionText', 'isValid','publisher')
    list_filter = ('publisher__userName',)
    search_fields = ['questionTitle']

    fieldsets = (
        ('Details', {
            'fields': ('questionTitle', 'questionText')
        }),
        ('Infos', {
            'classes': ('collapse',),
            'fields': ('isValid', 'publisher'),
        }),
    )

    #fields = ('questionTitle', 'questionText', ('isValid','publisher'))
    inlines = [TagInlines, CommentInlines]

    actions = ['set_to_valid']

    def set_to_valid(self, request, queryset):
        queryset.update(isValid=True)
    set_to_valid.short_description = 'Validate'


admin.site.register(Answer)
admin.site.register(Tag)
admin.site.register(Member,MemberAdmin)
admin.site.register(Question, QuestionAdmin)
